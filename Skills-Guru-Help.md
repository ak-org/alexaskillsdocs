
![alt text](https://s3-us-west-2.amazonaws.com/stock-guru/skillIcon.png "Stocks Guru")


## Introduction

Stocks Guru, an Alexa skill, makes it easier to find and track stock prices of companies that interest you. This guide contains description of various commands used in the skill.
If you are a new user, this guide is perfect starting point for you. The guide is divided in three parts

* New User setup
* Day to Day Usage Scenarios
* Limitations


## Features
The Stocks Guru is a conversation bot. Once you launch it, you can speak commands as a natural conversation without having to use invocation phrases over and over again. Alexa will stay in the skill till you close the conversation with a closing phrases or Alexa listener times out.

Important Note : Skills will let you save up to 10 stocks in a list
---------------------------------------------------------------
The Stocks Guru skill offers following Intents :

* Launch Intent

> Alexa, open stocks Guru

In the rest of the section, it is assumed that you have asked Alexa to open the skill by saying "Alexa, Open Stocks Guru".

* Whether market is open or close

> is stock exchange Open?

or

> is stock exchange close?

* Readout name of company stocks in your list

> what stocks are on my list?

or

> tell me about my stocks list

* Create default stocks list

> recreate default stocks list

or

> regenerate default stocks list

* Reset default stocks list

> reset my stocks list

or

> clear my stocks list

or

> nuke my stocks list

* Add a stock to your list
Assuming, you want to add amazon stock

>  add amazon

or

>  include amazon

* Remove a stock from your list
Assuming, you want to remove amazon

> purge amazon

or

> delete amazon

Assuming you want to remove apple stock

> purge apple

or

> delete apple

* Learn more about stock in your list

Assuming, you want to learn about netflix stock and it is in your list

> describe netflix

* Lookup stock symbol of a company

You can use the following command to confirm that company of your interest indeed is traded in US Stock market. if successful, you will hear about stock symbol. Ofcourse, replace Netflix with a company name of your choice

>look up netflix

* If specific stock is higher or lower (stock must be in your list)
Assuming, you have netflix's NFLX stock in your list, you will saying

> is netflix up or down

or

> was netflix up or down

or

> is netflix trading higher or lower?

Ofcourse, replace Netflix with a company name of your choice

* If market index is higher or lower

> is market index up or down?

or

> is market index high or low?

* If stock exchange is open

> is stock exchange Open?

or

> is stock exchange close?

* Close session  

> good bye

This command will close out our session. Just use following command to open a new session with the skill.

> Alexa, Open stocks guru


## How to use the skills?
* Step 1 - Ensure that you have granted requested permission in the Alexa companion app. This step is a pre-requisite for successfully using the skill.
* Step 2 - Create stocks list by adding one stock at a time or by creating default list
* Step 3 - Enjoy the simplicity of the skill

## Session Examples

###  New User Setup
** Allow skill to access your name and email address **
When you enable the skill in the Alexa companion app, Alexa app will prompt you to grant permissions to share name and email address. Please grant the Permissions or  the skill will not work.

### How to create default list and replace apple with ford stock
Below is a sample of commands you can use to create your default stocks list and replace stocks

> open stocks guru

> create default stocks list

> what stocks are on my list

> purge apple

> add ford motors

> what stocks are on my list

> is ford up or down

> what about market index

> good bye


###  Day to Day Usage

Let's assume that you have ford, apple and amazon stocks in our list. The interaction will look something like this:

> open stocks guru

> what stocks are on my list

> is ford up or down

> is apple up or down

> what about market index

> good bye


### Wipe out the current/default stocks list and add new stocks

> open stocks guru

> reset my stocks list

> what stocks are on my list

> add  apple

> add ford motors

> add twitter

> add amazon

> add face book

> what stocks are on my list

> is ford up or down

> what about market index

> is facebook trading higher or lower

> good bye

## Limitations

The input text is extracted from spoken language. Alexa has come a long way in understanding the natural language but it is possible that your words are
not understood properly or wrong intent is fired. If that is case, feel free to close the skill, re-open and try your command again.

The skill uses company name to look up for stock symbols database consisting of 8000+ entries. If company name is ambigous, it will pick the first match. In the background, it uses AMAZON.Corporation intent type. If intent does not capture the name appropriately, the match may not work. It is partially limitation of how company name themselves and how we humans pronounce their name.

__ If you are using different phrase to invoke your echo device or alexa application, simply replace Alexa with the phrase you use. __
