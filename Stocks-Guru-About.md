
![alt text](https://s3-us-west-2.amazonaws.com/stock-guru/skillIcon.png "Stocks Guru")


## Introduction

Stocks Guru, an Alexa skill, makes it easier to find and track stock prices of companies that interest you. Typical finance websites assumes
that you have financial background and you are reasonably comfortable with stock tickers, earnings, graphs etc. However, if you want a simple
straight forward way to keep track of stock prices relevant to you without the upsell of stories, blogs, podcasts or analysis, this skill is for you.

### Inspiration for the skills
All financial institutes have Alexa skills but none has been created with simplicity in mind. Stocks Guru has been created grounds up with simplicity in mind.

## Demo Video

[!["Stocks Guru skills Demo"](https://s3-us-west-2.amazonaws.com/stock-guru/vid-screenshot.png)](https://youtu.be/U_q-n8KRIBQ "Stocks Guru skills Demo")

## Architecture
The skill architecture and designs follows voice first philosophy. The information on the screen compliments the information being spoken by Alexa.
The skill is built on AWS server-less services. It leverages following technologies:

* AWS Lambda Function written in JavaScript to handle intent requests and send responses in JSON format
* AWS Dynamodb for backend database to store user's stocks list
* Intent confirmation through dialog delegation to facilitate conversational interaction
* Alexa Presentation Language (APL) to display information on alexa enabled devices with screens
* External APIs to get information about stocks and market indexes in real time

## Terms of use
The skill relies on external APIs for financial data. No warranty is made or implied for the accuracy of the data. The users assumes all risk associated with using the information provided by this skill.

## Permissions requirements
Please allow the skill to read your name and email address on the companion Alexa App. The skill **will not work** properly without access to requested data.

## Availability
The skill is built using server-less technologies. The availability is directly tied to the availability of AWS services (which has pretty good track record) and availability of financial data APIs. No formal SLA is guaranteed or implied.

## Privacy Policy

Your name and email address are your property. Your personal information will not shared or sold to used for any other commercial purposes. The name is used to personalize the experience and your email address is used to uniquely identify you in the database. We may use the data to do anonymous analytics work to further improve the skill.
The data is exchanged between your echo/Alexa device and AWS data centers. Amazon Web Service has its own policies for storing, archiving and managing user's data. Please refer to AWS website for details about their policies.

## Pricing
This skill contains *NO advertisements* and completely free to use. There are no strings attached to using this skill. :-)

## Credits
### Image Icons

* [FlatIcon]("https://www.flaticon.com/")

### Financial data
* [IExchange](https://www.iexchange.com)
* [AlphaVantage](https://www.alphavantage.co)
